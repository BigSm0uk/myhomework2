﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
        /// <summary>
        /// Удалить сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployee(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            
            if (employee == null)
                return NotFound();

            await _employeeRepository.DeleteAsync(id);

            return Ok();
        }
        /// <summary>
        /// Обновить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult> UpdateEmployee(Guid id, EmployeeRequest emp)
        {
            if (emp == null)
                return NotFound();
            var employeeToEdit = await _employeeRepository.GetByIdAsync(id);
            if (employeeToEdit == null)
                return NotFound(); 
            await _employeeRepository.UpdateAsync(id, new Employee()
            {
                Id = id,
                FirstName = emp.FirstName,
                LastName = emp.LastName,
                Email = emp.Email,
                Roles = emp.Roles.Select(x => new Role()
                {
                    Id = id

                }).ToList(),
                AppliedPromocodesCount = emp.AppliedPromocodesCount
            });
            return Ok();
        }
        /// <summary>
        /// Создать сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> CreateEmployee(EmployeeRequest emp)
        {
            await _employeeRepository.CreateAsync(new Employee()
            {
                Id = new Guid(),
                FirstName = emp.FirstName,
                LastName = emp.LastName,
                Email = emp.Email,
                Roles = emp.Roles.Select(x => new Role()
                {
                    Id = new Guid()
                }).ToList(),
                AppliedPromocodesCount = emp.AppliedPromocodesCount
            });
            return Ok();
        }
    }
}