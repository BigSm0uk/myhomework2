﻿using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<RoleRequest> Roles { get; set; }
        public int AppliedPromocodesCount { get; set; }
    }

    public class RoleRequest
    {
        public string Id { get; set; }
    }
}