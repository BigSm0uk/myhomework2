﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task DeleteAsync(Guid id)
        {
            var dataList = Data.ToList();
            var index = dataList.FindIndex(k => k.Id == id);
            dataList.RemoveAt(index);
            Data = dataList;
            return Task.CompletedTask;

        }

        public Task UpdateAsync(Guid id, T item)
        { 
            var dataList = Data.ToList();
            var index = dataList.FindIndex(k => k.Id == id);
            dataList[index]= item;
            Data = dataList;
            return Task.CompletedTask;
        }

        public Task CreateAsync(T item)
        {
            var dataList = Data.ToList();
            dataList.Add(item);
            Data = dataList;
            return Task.CompletedTask;
        }
    }
}